/*
 * read_encoder.c: simple test application
 *
 * This file contains a design example using the AXI GPIO driver (XGpio) and
 * hardware device.  It only uses channel 1 of a GPIO device.
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "sleep.h"
#include <xgpio.h>
#include "xparameters.h"

#define ENC_NUM 3       // Currently we are reading the data from three encoders.

XGpio ENC[ENC_NUM];

int main()
{

	// int DEVICE_ID[ENC_NUM] = {XPAR_ENC_MODULE_1_DEVICE_ID, XPAR_ENC_MODULE_2_DEVICE_ID, XPAR_ENC_MODULE_3_DEVICE_ID, XPAR_ENC_MODULE_4_DEVICE_ID, XPAR_ENC_MODULE_5_DEVICE_ID, XPAR_ENC_MODULE_6_DEVICE_ID, XPAR_ENC_MODULE_7_DEVICE_ID, XPAR_ENC_MODULE_8_DEVICE_ID, XPAR_ENC_MODULE_9_DEVICE_ID, XPAR_ENC_MODULE_10_DEVICE_ID, XPAR_ENC_MODULE_11_DEVICE_ID, XPAR_ENC_MODULE_12_DEVICE_ID, XPAR_ENC_MODULE_13_DEVICE_ID, XPAR_ENC_MODULE_14_DEVICE_ID, XPAR_ENC_MODULE_15_DEVICE_ID, XPAR_ENC_MODULE_16_DEVICE_ID};
	int DEVICE_ID[ENC_NUM] = {XPAR_ENC_MODULE_1_DEVICE_ID, XPAR_ENC_MODULE_2_DEVICE_ID, XPAR_ENC_MODULE_3_DEVICE_ID};
	for(int i = 0; i<ENC_NUM;i++)
	{
		XGpio_Initialize(&ENC[i], DEVICE_ID[i]);
		XGpio_SetDataDirection(&ENC[i], 1, 0x3FFFFFFF);
	}
	init_platform();
    while(1)
    {
	printf("%lu %lu %lu\n\r", XGpio_DiscreteRead(&ENC[0], 1), XGpio_DiscreteRead(&ENC[1], 1), XGpio_DiscreteRead(&ENC[2], 1));
	}
    cleanup_platform();
    return 0;
}
