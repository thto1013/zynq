----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.02.2019 14:17:02
-- Design Name: 
-- Module Name: position_generator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;                                                   -- for writing the bit values into a vector


entity position_generator is
    Port ( clk_in : in STD_LOGIC;
           position_bit : out STD_LOGIC;
           position_vector : out STD_LOGIC_VECTOR (29 downto 0);
           interrupt_bit : out STD_LOGIC);
end position_generator;


architecture Behavioral of position_generator is

constant clk_length : integer := 33; 
signal clk_count : integer := 0;
signal flag :std_logic := '1';
signal pos :std_logic;
signal pos_count : integer := 0;
signal pos_count_vector : std_logic_vector(29 downto 0) := "000000000000000000000000000000";
signal binary_vector: std_logic_vector(29 downto 0) := "000000000000000000000000000000";
signal modified_binary_vector: std_logic_vector(29 downto 0) := "000000000000000000000000000000";

begin

clock_count_process: process(clk_in)
begin
    if rising_edge(clk_in) then
        if clk_count = clk_length then           
            clk_count <= clk_count - clk_length;
        else
            clk_count <= clk_count + 1;
        end if;
    end if;     
end process;

position_count_process:process(clk_in)                                      -- implements a position counter that increments for each ssi clock cycle
begin
    if rising_edge(clk_in) then
        if clk_count = 1 then
            pos_count <= pos_count + 1;
        end if;
    end if;
end process;
pos_count_vector <= std_logic_vector(to_unsigned(pos_count, pos_count_vector'LENGTH));

incremental_data_process:process(clk_in)                                    -- splits the generated positions into 30 bits
begin
    if rising_edge(clk_in) then
        case clk_count is
        when 1 => pos <= pos_count_vector(29);
        when 2 => pos <= pos_count_vector(28);
        when 3 => pos <= pos_count_vector(27);
        when 4 => pos <= pos_count_vector(26);
        when 5 => pos <= pos_count_vector(25);
        when 6 => pos <= pos_count_vector(24);
        when 7 => pos <= pos_count_vector(23);
        when 8 => pos <= pos_count_vector(22);
        when 9 => pos <= pos_count_vector(21);
        when 10 => pos <= pos_count_vector(20);
        when 11 => pos <= pos_count_vector(19);
        when 12 => pos <= pos_count_vector(18);
        when 13 => pos <= pos_count_vector(17);
        when 14 => pos <= pos_count_vector(16);
        when 15 => pos <= pos_count_vector(15);
        when 16 => pos <= pos_count_vector(14);
        when 17 => pos <= pos_count_vector(13);
        when 18 => pos <= pos_count_vector(12);
        when 19 => pos <= pos_count_vector(11);
        when 20 => pos <= pos_count_vector(10);
        when 21 => pos <= pos_count_vector(9);
        when 22 => pos <= pos_count_vector(8);
        when 23 => pos <= pos_count_vector(7);
        when 24 => pos <= pos_count_vector(6);
        when 25 => pos <= pos_count_vector(5);
        when 26 => pos <= pos_count_vector(4);
        when 27 => pos <= pos_count_vector(3);
        when 28 => pos <= pos_count_vector(2);
        when 29 => pos <= pos_count_vector(1);
        when 30 => pos <= pos_count_vector(0);
        when others => pos <= '1';
        end case;
    end if;
    position_bit <= pos;
end process;

vector_process: process(clk_in)                                             -- writes the individual bits into a vector 
begin
    if falling_edge(clk_in) then
        case clk_count is
        when 2 => binary_vector(29) <= pos;
        when 3 => binary_vector(28) <= pos;
        when 4 => binary_vector(27) <= pos;
        when 5 => binary_vector(26) <= pos;
        when 6 => binary_vector(25) <= pos;
        when 7 => binary_vector(24) <= pos;
        when 8 => binary_vector(23) <= pos;
        when 9 => binary_vector(22) <= pos;
        when 10 => binary_vector(21) <= pos;
        when 11 => binary_vector(20) <= pos;
        when 12 => binary_vector(19) <= pos;
        when 13 => binary_vector(18) <= pos;
        when 14 => binary_vector(17) <= pos;
        when 15 => binary_vector(16) <= pos;
        when 16 => binary_vector(15) <= pos;
        when 17 => binary_vector(14) <= pos;
        when 18 => binary_vector(13) <= pos;
        when 19 => binary_vector(12) <= pos;
        when 20 => binary_vector(11) <= pos;
        when 21 => binary_vector(10) <= pos;
        when 22 => binary_vector(9) <= pos;
        when 23 => binary_vector(8) <= pos;
        when 24 => binary_vector(7) <= pos;
        when 25 => binary_vector(6) <= pos;
        when 26 => binary_vector(5) <= pos;
        when 27 => binary_vector(4) <= pos;
        when 28 => binary_vector(3) <= pos;
        when 29 => binary_vector(2) <= pos;
        when 30 => binary_vector(1) <= pos;
        when 31 => binary_vector(0) <= pos;
        when others => binary_vector <= binary_vector;
        end case;
    end if;
end process;

write_data_process: process(clk_in)                                             -- writes the data into a vector before reading the next data                 
begin
    if falling_edge(clk_in) and clk_count = 0 then                              -- represents the end of current data+delay cycle and the beginning of the next data
    modified_binary_vector <= binary_vector;
    end if;
position_vector <= modified_binary_vector;
end process;

interrupt_process: process(clk_in)                                              -- sets an interrupt signal at the end of data transmission  
begin
    if rising_edge(clk_in) then
        if clk_count >= 0 and clk_count < 33 then
            flag <= '0';
        elsif clk_count = 33 then    
            flag <= '1';
        end if;
    end if;
interrupt_bit <= flag;
end process;


end Behavioral;
