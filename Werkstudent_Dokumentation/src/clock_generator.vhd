----------------------------------------------------------------------------------
-- Company: SICK STEGMANN GmbH
-- Engineer: Tony Thomas
-- 
-- Create Date: 22.11.2018 16:57:21
-- Design Name: design_1
-- Module Name: clock_generator - Behavioral
-- Project Name: cable_robotics
-- Target Devices: MYIR Z-Turn Board
-- Tool Versions: Vivado 2018.2
-- Description: The file generates a 1MHz ssi clock with a significant delay. 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;                                    -- for writing the counter value into a vector  


entity clock_generator is
    Port ( clock_in : in STD_LOGIC;                          -- clock_in : 2 MHz input clock
           clock_out : out STD_LOGIC);                        -- clock_out : 1 MHz output ssi clock
end clock_generator;


architecture Behavioral of clock_generator is
constant clock_cycles : integer := 68;           -- 68 is used for representing 33 clock cycles(33 bits of data * 2 + 2). 
constant delay_cycles: integer := 321;           -- delay considering the simultaneous reading of 16 sensor data  
begin

clock_process: process(clock_in)                       
variable counter_1 : integer := 0;
variable temp : std_logic := '1';
begin
    if rising_edge(clock_in) then
        counter_1 := counter_1 + 1;
        if counter_1 < clock_cycles then                           
            temp := not temp;
        elsif counter_1 >= clock_cycles and counter_1 < delay_cycles then          
            temp := '1';
        else
            counter_1 := 0;
        end if;
    end if;    
    clock_out <= temp;                                  
end process;

end Behavioral;
