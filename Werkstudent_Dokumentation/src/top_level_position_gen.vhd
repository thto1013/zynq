----------------------------------------------------------------------------------
-- Company: SICK STEGMANN GmbH
-- Engineer: Tony Thomas
-- 
-- Create Date: 05.02.2019 12:58:43
-- Design Name: design_1
-- Module Name: top_level - Behavioral
-- Project Name: cable_robotics
-- Target Devices: MYIR Z-Turn Board
-- Tool Versions: Vivado 2018.2
-- Description: The system and ssi clock can be visualized on the two GPIO pins on the Z-turn IO Cape.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Port description:
--  system_clock      : FCLK internal clock of ZYNQ(2 MHz)
--  ssi_clock         : SSI system clock of 1 MHz
--  interrupt         : Denotes the succesful data transmission
--  enc_n_in          : 30 bit grey output of the n'th encoder  
--  position_bit_n    : 30 bit binary output of the n'th encoder
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity top_level is
    Port ( system_clock : out STD_LOGIC;
           ssi_clock : out STD_LOGIC;
           interrupt : out STD_LOGIC; 
           position_bit_1 : out STD_LOGIC;
           FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
           FIXED_IO_ps_clk : inout STD_LOGIC;
           FIXED_IO_ps_porb : inout STD_LOGIC;
           FIXED_IO_ps_srstb : inout STD_LOGIC;
		   DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
		   DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
		   DDR_cas_n : inout STD_LOGIC;
           DDR_ck_n : inout STD_LOGIC;
           DDR_ck_p : inout STD_LOGIC;
           DDR_cke : inout STD_LOGIC;
           DDR_cs_n : inout STD_LOGIC;
		   DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
		   DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
		   DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
		   DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
		   DDR_odt : inout STD_LOGIC;
		   DDR_ras_n : inout STD_LOGIC;
		   DDR_reset_n : inout STD_LOGIC;
		   DDR_we_n : inout STD_LOGIC;
		   FIXED_IO_ddr_vrn : inout STD_LOGIC;
		   FIXED_IO_ddr_vrp : inout STD_LOGIC
           );
end top_level;

architecture Behavioral of top_level is

signal clk_system : std_logic;
signal clk_ssi : std_logic; 
signal interrupt_signal : std_logic;
signal position_bit_signal : std_logic;
signal position_vector_signal : std_logic_vector(29 downto 0);
signal position_signal_1 : std_logic_vector(29 downto 0);
signal position_signal_2 : std_logic_vector(29 downto 0);
signal position_signal_3 : std_logic_vector(29 downto 0);
signal position_signal_4 : std_logic_vector(29 downto 0);
signal position_signal_5 : std_logic_vector(29 downto 0);
signal position_signal_6 : std_logic_vector(29 downto 0);
signal position_signal_7 : std_logic_vector(29 downto 0);
signal position_signal_8 : std_logic_vector(29 downto 0);
signal position_signal_9 : std_logic_vector(29 downto 0);
signal position_signal_10 : std_logic_vector(29 downto 0);
signal position_signal_11 : std_logic_vector(29 downto 0);
signal position_signal_12 : std_logic_vector(29 downto 0);
signal position_signal_13 : std_logic_vector(29 downto 0);
signal position_signal_14 : std_logic_vector(29 downto 0);
signal position_signal_15 : std_logic_vector(29 downto 0);
signal position_signal_16 : std_logic_vector(29 downto 0);

component design_1_wrapper is
  port (
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FCLK : out STD_LOGIC;
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    INTR : in STD_LOGIC_VECTOR ( 0 to 0 );
    encoder_1 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_11 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_10 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_12 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_13 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_14 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_2 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_3 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_4 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_5 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_6 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_15 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_16 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_7 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_8 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_9 : in STD_LOGIC_VECTOR ( 29 downto 0 )
  );
end component;

component clock_generator is
    Port ( clk_in : in STD_LOGIC;                          
           clk_out : out STD_LOGIC);                       
end component;

component position_generator is
    Port ( clk_in : in STD_LOGIC;
           position_bit : out STD_LOGIC;
           position_vector : out STD_LOGIC_VECTOR (29 downto 0);
           interrupt_bit : out STD_LOGIC);
end component;


begin

wrapper: design_1_wrapper
     port map (
     FCLK => clk_system,
     FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
     FIXED_IO_ps_clk => FIXED_IO_ps_clk,
     FIXED_IO_ps_porb => FIXED_IO_ps_porb,
     FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
     INTR(0) => interrupt_signal,
     DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
     DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
	 DDR_cas_n => DDR_cas_n,
	 DDR_ck_n => DDR_ck_n,
	 DDR_ck_p => DDR_ck_p,
	 DDR_cke => DDR_cke,
	 DDR_cs_n => DDR_cs_n,
	 DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
	 DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
	 DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
	 DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
	 DDR_odt => DDR_odt,
	 DDR_ras_n => DDR_ras_n,
	 DDR_reset_n => DDR_reset_n,
	 DDR_we_n => DDR_we_n,
     FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
     FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
     encoder_1(29 downto 0) => position_vector_signal,
     encoder_2(29 downto 0) => position_signal_2,
     encoder_3(29 downto 0) => position_signal_3,
     encoder_4(29 downto 0) => position_signal_4,
     encoder_5(29 downto 0) => position_signal_5,
     encoder_6(29 downto 0) => position_signal_6,
     encoder_7(29 downto 0) => position_signal_7,
     encoder_8(29 downto 0) => position_signal_8,
     encoder_9(29 downto 0) => position_signal_9,
     encoder_10(29 downto 0) => position_signal_10,
     encoder_11(29 downto 0) => position_signal_11,
     encoder_12(29 downto 0) => position_signal_12,
     encoder_13(29 downto 0) => position_signal_13,
     encoder_14(29 downto 0) => position_signal_14,
     encoder_15(29 downto 0) => position_signal_15,
     encoder_16(29 downto 0) => position_signal_16
);

clk_gen: clock_generator         
    port map (
    clk_in => clk_system,                  
    clk_out => clk_ssi
    );     

pos_gen: position_generator
    port map (
    clk_in => clk_ssi,                  
    position_bit => position_bit_signal,
    position_vector => position_vector_signal,
    interrupt_bit => interrupt_signal
    );

system_clock <= clk_system;  
ssi_clock <= clk_ssi;
interrupt <= interrupt_signal;
position_bit_1 <= position_bit_signal;
end Behavioral;
