set_property PACKAGE_PIN A20 [get_ports {system_clock}]
set_property IOSTANDARD LVCMOS33 [get_ports {system_clock}]
set_property PACKAGE_PIN D18 [get_ports {ssi_clock}]
set_property IOSTANDARD LVCMOS33 [get_ports {ssi_clock}]

set_property PACKAGE_PIN D20 [get_ports {position_bit_1}]
set_property IOSTANDARD LVCMOS33 [get_ports { position_bit_1}]
set_property PACKAGE_PIN E19 [get_ports {position_bit_2}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_2}]
set_property PACKAGE_PIN F17 [get_ports {position_bit_3}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_3}]
set_property PACKAGE_PIN M20 [get_ports {position_bit_4}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_4}]
set_property PACKAGE_PIN M18 [get_ports {position_bit_5}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_5}]
set_property PACKAGE_PIN L20 [get_ports {position_bit_6}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_6}]
set_property PACKAGE_PIN J19 [get_ports {position_bit_7}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_7}]
set_property PACKAGE_PIN L17 [get_ports {position_bit_8}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_8}]
set_property PACKAGE_PIN K18 [get_ports {position_bit_9}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_9}]
set_property PACKAGE_PIN H18 [get_ports {position_bit_10}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_10}]
set_property PACKAGE_PIN F20 [get_ports {position_bit_11}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_11}]
set_property PACKAGE_PIN G18 [get_ports {position_bit_12}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_12}]
set_property PACKAGE_PIN H20 [get_ports {position_bit_13}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_13}]
set_property PACKAGE_PIN G20 [get_ports {position_bit_14}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_14}]
set_property PACKAGE_PIN G15 [get_ports {position_bit_15}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_15}]
set_property PACKAGE_PIN J14 [get_ports {position_bit_16}]
set_property IOSTANDARD LVCMOS33 [get_ports {position_bit_16}]

set_property PACKAGE_PIN K14 [get_ports {interrupt}]
set_property IOSTANDARD LVCMOS33 [get_ports {interrupt}]

set_property PACKAGE_PIN B19 [get_ports {enc_1_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_1_in}]
set_property PACKAGE_PIN E17 [get_ports {enc_2_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_2_in}]
set_property PACKAGE_PIN D19 [get_ports {enc_3_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_3_in}]
set_property PACKAGE_PIN E18 [get_ports {enc_4_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_4_in}]
set_property PACKAGE_PIN F16 [get_ports {enc_5_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_5_in}]
set_property PACKAGE_PIN M19 [get_ports {enc_6_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_6_in}]
set_property PACKAGE_PIN M17 [get_ports {enc_7_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_7_in}]
set_property PACKAGE_PIN L19 [get_ports {enc_8_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_8_in}]
set_property PACKAGE_PIN K19 [get_ports {enc_9_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_9_in}]
set_property PACKAGE_PIN L16 [get_ports {enc_10_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_10_in}]
set_property PACKAGE_PIN H17 [get_ports {enc_11_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_11_in}]
set_property PACKAGE_PIN J18 [get_ports {enc_12_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_12_in}]
set_property PACKAGE_PIN F19 [get_ports {enc_13_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_13_in}]
set_property PACKAGE_PIN G17 [get_ports {enc_14_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_14_in}]
set_property PACKAGE_PIN J20 [get_ports {enc_15_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_15_in}]
set_property PACKAGE_PIN G19 [get_ports {enc_16_in}]
set_property IOSTANDARD LVCMOS33 [get_ports {enc_16_in}]
