
#include <stdio.h>
#include <stdlib.h>
#include "xil_io.h"
#include "xil_exception.h"
#include "xil_cache.h"
#include "xil_types.h"
#include "xscugic.h"
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <assert.h>
#include "circular_buffer.h"
#include "platform.h"
#include "xil_printf.h"
#include "sleep.h"
#include <xgpio.h>
#include "xparameters.h"

XGpio ENC[16];
int interrupt_count = 0;
#define EXAMPLE_BUFFER_SIZE 1600                                                            // upper limit for the circular buffer size
#define DATA_SIZE 100                                                                       // number of positions to be read
#define ENC_NUM 16                                                                          // number of encoders
#define INTC_DEVICE_ID		XPAR_PS7_SCUGIC_0_DEVICE_ID                                        
#define INTC_DEVICE_INT_ID	XPAR_FABRIC_INT_IN_INTR

int ScuGicExample(u16 DeviceId);
int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr);
void DeviceDriverHandler(void *CallbackRef);
XScuGic InterruptController;
static XScuGic_Config *GicConfig;
volatile static int InterruptProcessed = FALSE;
static uint32_t datas[16][200];

void ReadEncoderPositionArray(void);
void StoreArray();


static void AssertPrint(const char8 *FilenamePtr, s32 LineNumber){
	xil_printf("ASSERT: File Name: %s ", FilenamePtr);
	xil_printf("Line Number: %d\r\n",LineNumber);
}

int main(void)
{

	int Status;
	Xil_AssertSetCallback(AssertPrint);
	Xil_DCacheDisable();
	Xil_ICacheDisable();


	int DEVICE_ID[16] = {XPAR_ENC_1_DEVICE_ID, XPAR_ENC_2_DEVICE_ID, XPAR_ENC_3_DEVICE_ID, XPAR_ENC_4_DEVICE_ID, XPAR_ENC_5_DEVICE_ID, XPAR_ENC_6_DEVICE_ID, XPAR_ENC_7_DEVICE_ID, XPAR_ENC_8_DEVICE_ID, XPAR_ENC_9_DEVICE_ID, XPAR_ENC_10_DEVICE_ID, XPAR_ENC_11_DEVICE_ID, XPAR_ENC_12_DEVICE_ID, XPAR_ENC_13_DEVICE_ID, XPAR_ENC_14_DEVICE_ID, XPAR_ENC_15_DEVICE_ID, XPAR_ENC_16_DEVICE_ID};
	for(int i = 0; i<ENC_NUM;i++)
	{
		XGpio_Initialize(&ENC[i], DEVICE_ID[i]);
		XGpio_SetDataDirection(&ENC[i], 1, 0x3FFFFFFF);
	}

	Status = ScuGicExample(INTC_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		printf("GIC Example Test Failed\r\n");
		return XST_FAILURE;
	}
	XScuGic_Enable(&InterruptController, INTC_DEVICE_INT_ID);
	return XST_SUCCESS;
}

int ScuGicExample(u16 DeviceId)
{
	int Status;

	GicConfig = XScuGic_LookupConfig(DeviceId);
	if (NULL == GicConfig) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(&InterruptController, GicConfig,
					GicConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}


	Status = XScuGic_SelfTest(&InterruptController);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Status = SetUpInterruptSystem(&InterruptController);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Status = XScuGic_Connect(&InterruptController, INTC_DEVICE_INT_ID,
			   (Xil_ExceptionHandler)DeviceDriverHandler,
			   (void *)&InterruptController);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}


int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr)
{
	Xil_ExceptionInit();           
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler) XScuGic_InterruptHandler,
			XScuGicInstancePtr);
	Xil_ExceptionEnable();
	return XST_SUCCESS;
}

void DeviceDriverHandler(void *CallbackRef)
{
	InterruptProcessed = TRUE;
	interrupt_count += 1;
	if (interrupt_count == 1)                                                                               // when interrupt_count = n, this will output 1600*n data into the terminal. However, each 1600 continuous dataset are not continuous.
		{
		StoreArray();
		}
	}


void ReadEncoderPositionArray(void)
{
	for(int i=0;i<DATA_SIZE;i++)
	{
		for(int j=0;j<ENC_NUM;j++)
		{
			datas[j][i] = XGpio_DiscreteRead(&ENC[j], 1);
		}
	}
}

void StoreArray()
{
	ReadEncoderPositionArray();
	uint32_t * buffer1  = malloc(EXAMPLE_BUFFER_SIZE * sizeof(uint32_t));
	cbuf_handle_t data1_buf = circular_buf_init(buffer1, EXAMPLE_BUFFER_SIZE);
	for(int i = 0; i < DATA_SIZE; i++)
		{
		for(int j = 0; j < ENC_NUM; j++)
			{
			circular_buf_put(data1_buf, datas[j][i]);
			}
		}
		while(!circular_buf_empty(data1_buf))
		{
			uint32_t data1;
			circular_buf_get(data1_buf, &data1);
			printf("%lu\n", data1);
		}
	free(buffer1);
	circular_buf_free(data1_buf);
}
