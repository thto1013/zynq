/*
 * position_counter.c: simple test application
 *
 * This file contains a design example using the AXI GPIO driver (XGpio) and
 * hardware device.  It only uses channel 1 of a GPIO device.
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "sleep.h"
#include <xgpio.h>
#include "xparameters.h"

XGpio ENC;

int main()
{
    int DEVICE_ID= XPAR_ENC_MODULE_1_DEVICE_ID;                // obtained from xparameters.h
    XGpio_Initialize(&ENC, DEVICE_ID_1);
    XGpio_SetDataDirection(&ENC, 1, 0x3FFFFFFF);
    init_platform();
    while(1)
    {
	printf("%lu\n\r", XGpio_DiscreteRead(&ENC, 1));
    }
    cleanup_platform();
    return 0;
}