----------------------------------------------------------------------------------
-- Company: SICK STEGMANN GmbH
-- Engineer: Tony Thomas
-- 
-- Create Date: 05.02.2019 12:58:43
-- Design Name: design_1
-- Module Name: top_level - Behavioral
-- Project Name: cable_robotics
-- Target Devices: MYIR Z-Turn Board
-- Tool Versions: Vivado 2018.2
-- Description: The system and ssi clock can be visualized on the two GPIO pins on the Z-turn IO Cape.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Port description:
--  system_clock      : FCLK internal clock of ZYNQ(2 MHz)
--  ssi_clock         : SSI system clock of 1 MHz
--  interrupt         : Denotes the succesful data transmission
--  enc_n_in          : 30 bit grey output of the n'th encoder  
--  position_bit_n    : 30 bit binary output of the n'th encoder
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity top_level is
    Port ( system_clock : out STD_LOGIC;
           ssi_clock : out STD_LOGIC;
           interrupt : out STD_LOGIC;
           enc_1_in :in STD_LOGIC;
           enc_2_in :in STD_LOGIC;
           enc_3_in :in STD_LOGIC;
           enc_4_in :in STD_LOGIC;
           enc_5_in :in STD_LOGIC;
           enc_6_in :in STD_LOGIC;
           enc_7_in :in STD_LOGIC;
           enc_8_in :in STD_LOGIC;
           enc_9_in :in STD_LOGIC;
           enc_10_in :in STD_LOGIC;
           enc_11_in :in STD_LOGIC;
           enc_12_in :in STD_LOGIC;
           enc_13_in :in STD_LOGIC;
           enc_14_in :in STD_LOGIC;
           enc_15_in :in STD_LOGIC;
           enc_16_in :in STD_LOGIC;           
           position_bit_1 : out STD_LOGIC;
           position_bit_2 : out STD_LOGIC;
           position_bit_3 : out STD_LOGIC;
           position_bit_4 : out STD_LOGIC;
           position_bit_5 : out STD_LOGIC;
           position_bit_6 : out STD_LOGIC;
           position_bit_7 : out STD_LOGIC;
           position_bit_8 : out STD_LOGIC;
           position_bit_9 : out STD_LOGIC;
           position_bit_10 : out STD_LOGIC;
           position_bit_11 : out STD_LOGIC;
           position_bit_12 : out STD_LOGIC;
           position_bit_13 : out STD_LOGIC;
           position_bit_14 : out STD_LOGIC;
           position_bit_15 : out STD_LOGIC;
           position_bit_16 : out STD_LOGIC;
           FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
           FIXED_IO_ps_clk : inout STD_LOGIC;
           FIXED_IO_ps_porb : inout STD_LOGIC;
           FIXED_IO_ps_srstb : inout STD_LOGIC);
end top_level;

architecture Behavioral of top_level is

signal clk_system : std_logic;
signal clk_ssi : std_logic; 
signal interrupt_signal : std_logic;
signal position_bit_signal : std_logic;
signal binary_signal_1 : std_logic;
signal binary_signal_2 : std_logic;
signal binary_signal_3 : std_logic;
signal binary_signal_4 : std_logic;
signal binary_signal_5 : std_logic;
signal binary_signal_6 : std_logic;
signal binary_signal_7 : std_logic;
signal binary_signal_8 : std_logic;
signal binary_signal_9 : std_logic;
signal binary_signal_10 : std_logic;
signal binary_signal_11 : std_logic;
signal binary_signal_12 : std_logic;
signal binary_signal_13 : std_logic;
signal binary_signal_14 : std_logic;
signal binary_signal_15 : std_logic;
signal binary_signal_16 : std_logic;
signal position_vector_signal : std_logic_vector(29 downto 0);
signal position_signal_1 : std_logic_vector(29 downto 0);
signal position_signal_2 : std_logic_vector(29 downto 0);
signal position_signal_3 : std_logic_vector(29 downto 0);
signal position_signal_4 : std_logic_vector(29 downto 0);
signal position_signal_5 : std_logic_vector(29 downto 0);
signal position_signal_6 : std_logic_vector(29 downto 0);
signal position_signal_7 : std_logic_vector(29 downto 0);
signal position_signal_8 : std_logic_vector(29 downto 0);
signal position_signal_9 : std_logic_vector(29 downto 0);
signal position_signal_10 : std_logic_vector(29 downto 0);
signal position_signal_11 : std_logic_vector(29 downto 0);
signal position_signal_12 : std_logic_vector(29 downto 0);
signal position_signal_13 : std_logic_vector(29 downto 0);
signal position_signal_14 : std_logic_vector(29 downto 0);
signal position_signal_15 : std_logic_vector(29 downto 0);
signal position_signal_16 : std_logic_vector(29 downto 0);

component design_1_wrapper is
  port (
    FCLK : out STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    INTR : in STD_LOGIC_VECTOR ( 0 to 0 );
    encoder_1 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_11 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_10 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_12 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_13 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_14 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_2 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_3 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_4 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_5 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_6 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_15 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_16 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_7 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_8 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    encoder_9 : in STD_LOGIC_VECTOR ( 29 downto 0 )
  );
end component;

component clock_generator is
    Port ( clk_in : in STD_LOGIC;                          
           clk_out : out STD_LOGIC);                       
end component;

component position_generator is
    Port ( clk_in : in STD_LOGIC;
           position_bit : out STD_LOGIC;
           position_vector : out STD_LOGIC_VECTOR (29 downto 0);
           interrupt_bit : out STD_LOGIC);
end component;

component single_encoder is
    Port ( clk_in : in STD_LOGIC;
           data_grey_bit : in STD_LOGIC;
           data_bin_bit : inout STD_LOGIC;
           data_bin_vector : out STD_LOGIC_VECTOR (29 downto 0));
end component;

begin

wrapper: design_1_wrapper
     port map (
     FCLK => clk_system,
     FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
     FIXED_IO_ps_clk => FIXED_IO_ps_clk,
     FIXED_IO_ps_porb => FIXED_IO_ps_porb,
     FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
     INTR(0) => interrupt_signal,
     --encoder_1(29 downto 0) => position_vector_signal,
     encoder_1(29 downto 0) => position_signal_1,
     encoder_2(29 downto 0) => position_signal_2,
     encoder_3(29 downto 0) => position_signal_3,
     encoder_4(29 downto 0) => position_signal_4,
     encoder_5(29 downto 0) => position_signal_5,
     encoder_6(29 downto 0) => position_signal_6,
     encoder_7(29 downto 0) => position_signal_7,
     encoder_8(29 downto 0) => position_signal_8,
     encoder_9(29 downto 0) => position_signal_9,
     encoder_10(29 downto 0) => position_signal_10,
     encoder_11(29 downto 0) => position_signal_11,
     encoder_12(29 downto 0) => position_signal_12,
     encoder_13(29 downto 0) => position_signal_13,
     encoder_14(29 downto 0) => position_signal_14,
     encoder_15(29 downto 0) => position_signal_15,
     encoder_16(29 downto 0) => position_signal_16
);

clk_gen: clock_generator         
    port map (
    clk_in => clk_system,                  
    clk_out => clk_ssi
    );     

pos_gen: position_generator
    port map (
    clk_in => clk_ssi,                  
    position_bit => position_bit_signal,
    position_vector => position_vector_signal,
    interrupt_bit => interrupt_signal
    );

enc_1: single_encoder
    port map (
    clk_in => clk_ssi,
    data_grey_bit => enc_1_in,
    data_bin_bit => binary_signal_1,
    data_bin_vector => position_signal_1
    );

enc_2: single_encoder
    port map (
    clk_in => clk_ssi,
    data_grey_bit => enc_2_in,
    data_bin_bit => binary_signal_2,
    data_bin_vector => position_signal_2
    );

enc_3: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_3_in,
	data_bin_bit => binary_signal_3,
	data_bin_vector => position_signal_3
	);

enc_4: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_4_in,
	data_bin_bit => binary_signal_4,
	data_bin_vector => position_signal_4
	);

enc_5: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_5_in,
	data_bin_bit => binary_signal_5,
	data_bin_vector => position_signal_5
	);

enc_6: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_6_in,
	data_bin_bit => binary_signal_6,
	data_bin_vector => position_signal_6
	);

enc_7: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_7_in,
	data_bin_bit => binary_signal_7,
	data_bin_vector => position_signal_7
	);

enc_8: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_8_in,
	data_bin_bit => binary_signal_8,
	data_bin_vector => position_signal_8
	);

enc_9: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_9_in,
	data_bin_bit => binary_signal_9,
	data_bin_vector => position_signal_9
	);

enc_10: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_10_in,
	data_bin_bit => binary_signal_10,
	data_bin_vector => position_signal_10
	);

enc_11: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_11_in,
	data_bin_bit => binary_signal_11,
	data_bin_vector => position_signal_11
	);

enc_12: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_12_in,
	data_bin_bit => binary_signal_12,
	data_bin_vector => position_signal_12
	);

enc_13: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_13_in,
	data_bin_bit => binary_signal_13,
	data_bin_vector => position_signal_13
	);

enc_14: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_14_in,
	data_bin_bit => binary_signal_14,
	data_bin_vector => position_signal_14
	);

enc_15: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_15_in,
	data_bin_bit => binary_signal_15,
	data_bin_vector => position_signal_15
	);

enc_16: single_encoder
	port map (
	clk_in => clk_ssi,
	data_grey_bit => enc_16_in,
	data_bin_bit => binary_signal_16,
	data_bin_vector => position_signal_16
	);

system_clock <= clk_system;  
ssi_clock <= clk_ssi;
interrupt <= interrupt_signal;
--position_bit_1 <= position_bit_signal;
position_bit_1 <= binary_signal_1;
position_bit_2 <= binary_signal_2;
position_bit_3 <= binary_signal_3;
position_bit_4 <= binary_signal_4;
position_bit_5 <= binary_signal_5;
position_bit_6 <= binary_signal_6;
position_bit_7 <= binary_signal_7;
position_bit_8 <= binary_signal_8;
position_bit_9 <= binary_signal_9;
position_bit_10 <= binary_signal_10;
position_bit_11 <= binary_signal_11;
position_bit_12 <= binary_signal_12;
position_bit_13 <= binary_signal_13;
position_bit_14 <= binary_signal_14;
position_bit_15 <= binary_signal_15;
position_bit_16 <= binary_signal_16;
end Behavioral;
