set_property PACKAGE_PIN A20 [get_ports {system_clock}]
set_property IOSTANDARD LVCMOS33 [get_ports {system_clock}]
set_property PACKAGE_PIN D18 [get_ports {ssi_clock}]
set_property IOSTANDARD LVCMOS33 [get_ports {ssi_clock}]