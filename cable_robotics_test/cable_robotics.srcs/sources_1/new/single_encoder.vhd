----------------------------------------------------------------------------------
-- Company: SICK STEGMANN GmbH
-- Engineer: Tony Thomas
-- 
-- Create Date: 07.02.2019 10:46:46
-- Design Name: design_1
-- Module Name: single_encoder - Behavioral
-- Project Name: cable_robotics
-- Target Devices: MYIR Z-turn Board
-- Tool Versions: Vivado 2018.2
-- Description: This file reads the grey data output from a single encoder and outputs the corresponding positions in binary format.  
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Port description:
--  clk_in            : SSI input clock 
--  data_grey_bit     : Grey data output from an encoder
--  data_bin_bit      : Binary output of the encoder
--  data_bin_vector   : 30 bit binary output of the encoder  
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity single_encoder is
    Port ( clk_in : in STD_LOGIC;
           data_grey_bit : in STD_LOGIC;
           data_bin_bit : inout STD_LOGIC;
           data_bin_vector : out STD_LOGIC_VECTOR (29 downto 0));
end single_encoder;


architecture Behavioral of single_encoder is

constant clock_length : integer := 33; 
signal clock_counter : integer := 0;
signal binary_vector : std_logic_vector(29 downto 0) := "000000000000000000000000000000";
signal modified_vector : std_logic_vector(29 downto 0) := "000000000000000000000000000000";

begin

clock_counter_process: process(clk_in)
begin
    if rising_edge(clk_in) then
        if clock_counter = clock_length then           
            clock_counter <= clock_counter - clock_length;
        else
            clock_counter <= clock_counter + 1;
        end if;
    end if;  
end process;

grey_to_binary_conversion:process(clk_in)
variable tmp : std_logic := '1';
begin
    if falling_edge(clk_in) then        
        if clock_counter = 1 then
            tmp  := data_grey_bit;
        elsif clock_counter > 1 and clock_counter <= clock_length-3 then
            tmp  :=  tmp  xor data_grey_bit;
        elsif clock_counter >= clock_length-2 and clock_counter <= clock_length-1 then
            tmp  :=  data_grey_bit;
        else
            tmp := '1';    
        end if; 
    end if;
    data_bin_bit <= tmp ; 
end process;

vector_process : process(clk_in)
begin
    if falling_edge(clk_in) then
        case clock_counter is
        when 2 => binary_vector(29) <= data_bin_bit;
        when 3 => binary_vector(28) <= data_bin_bit;
        when 4 => binary_vector(27) <= data_bin_bit;
        when 5 => binary_vector(26) <= data_bin_bit;
        when 6 => binary_vector(25) <= data_bin_bit;
        when 7 => binary_vector(24) <= data_bin_bit;
        when 8 => binary_vector(23) <= data_bin_bit;
        when 9 => binary_vector(22) <= data_bin_bit;
        when 10 => binary_vector(21) <= data_bin_bit;
        when 11 => binary_vector(20) <= data_bin_bit;
        when 12 => binary_vector(19) <= data_bin_bit;
        when 13 => binary_vector(18) <= data_bin_bit;
        when 14 => binary_vector(17) <= data_bin_bit;
        when 15 => binary_vector(16) <= data_bin_bit;
        when 16 => binary_vector(15) <= data_bin_bit;
        when 17 => binary_vector(14) <= data_bin_bit;
        when 18 => binary_vector(13) <= data_bin_bit;
        when 19 => binary_vector(12) <= data_bin_bit;
        when 20 => binary_vector(11) <= data_bin_bit;
        when 21 => binary_vector(10) <= data_bin_bit;
        when 22 => binary_vector(9) <= data_bin_bit;
        when 23 => binary_vector(8) <= data_bin_bit;
        when 24 => binary_vector(7) <= data_bin_bit;
        when 25 => binary_vector(6) <= data_bin_bit;
        when 26 => binary_vector(5) <= data_bin_bit;
        when 27 => binary_vector(4) <= data_bin_bit;
        when 28 => binary_vector(3) <= data_bin_bit;
        when 29 => binary_vector(2) <= data_bin_bit;
        when 30 => binary_vector(1) <= data_bin_bit;
        when 31 => binary_vector(0) <= data_bin_bit;
        when others => binary_vector <= binary_vector;
        end case;
    end if;
end process;

actual_binary : process(clk_in)
begin
    if falling_edge(clk_in) and clock_counter = 0 then
        modified_vector <= binary_vector;
    end if;
    data_bin_vector <= modified_vector;
end process;


end Behavioral;
